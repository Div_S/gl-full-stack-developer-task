# GL Full Stack Developer Task

## Question

The "Game of Dice" is a multiplayer game where N players roll a 6 faced dice in a round-robin
fashion. Each time a player rolls the dice their points increase by the number (1 to 6) achieved
by the roll.

As soon as a player accumulates M points they complete the game and are assigned a rank.
Remaining players continue to play the game till they accumulate at least M points. The game
ends when all players have accumulated at least M points.

Rules of the game
- The order in which the users roll the dice is decided randomly at the start of the game.
- If a player rolls the value "6" then they immediately get another chance to roll again and move
ahead in the game.
- If a player rolls the value "1" two consecutive times then they are forced to skip their next turn
as a penalty.


## Implementation 

Language used : CPP

Idea :

All the information needed for every player is stored as a object . Initially random ordering is obtained by giving the player names in random order in consecutive indices .
The first random player , is prompted to roll the die and correspondingly checks for 1's
(to keep track of consecutive ones for a penalty) and the roll of the die gets added to the score of the player which determines the rank.
If the player has reached the threshold score ,the player is marked completed where in he does not get further turns .
If the current roll is a 6,the player get's another chance . Finally the ranks of the player's are updated.

## How to run the code ?

### Using a editor 

If your using any editor import the code in your editor .

Compile -> run

### In command line

1) g++ GameOfDice.c++
2) ./a.out

### Performance

Time COmplexity = O(n^2 log n)

n->Number of players

where nLOgn is the time taken to arrange rankwise after every turn

Space Complexity = O(n)

n->Number of PLayers 

