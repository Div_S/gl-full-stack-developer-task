#include<bits/stdc++.h>
using namespace std;

// A class to store details about the player after every turn
class GameOfDice{

    public:
    
        int playerName; // Name of the player
        bool isCompleted; //Denotes whether the player has complted the game
        int score; // Score of the current player as of the turn
        int rankScore; // Rankscore is used to calculate rank which is holds the same value of score except when it has completed.
        int rank; //Rank of the player
        int rollOne; //To keep track of consecutive one's rolled

    
        
        GameOfDice(){

        }


        GameOfDice(int player){
            playerName=player;
            isCompleted=false;
            score=0;
            rankScore=0;
            rank=0;
            rollOne=0;

        }
};

//Custom sort for calculating rank based on score
/*
Once the player completes the game the rankscore get's updated to the max score the player
has to reach to complete the game.

Reasoning :
    By doing this , the rank get's alloted on first come first serve basis.
    For e.g : If Player-1 completes first with score 10 (say threshold score)
    and Player-2 completes second with score 15 .This way Player-1 is given rank-1.


*/
bool operator<(GameOfDice const & a,GameOfDice const & b)
{
    if(a.rankScore == b.rankScore)
        return a.rank < b.rank;
    return a.rankScore > b.rankScore;
}

//Helper function which returns the random order in which players should start rolling the die.
vector<int> getRandomOrder(int n)
{
    vector<int> order;

    for(int i=1;i<=n;i++)
        order.push_back(i);
    
    random_shuffle(order.begin(),order.end());

    return order;
}

//Helper function to get die roll
int getDieRoll(){
   
    return 1 + (rand() % 6);

}

//Function to print scores and ranks after every turn
void printScores(vector<GameOfDice> players,int n)
{
    cout<<"The scores of players as of now :"<<endl;

    vector<GameOfDice> sorted = players;
    sort(sorted.begin(),sorted.end());


    for(int i=0;i<n;i++)
    {
        sorted[i].rank=i+1;
        cout<<"Rank "<<sorted[i].rank<<":"<<endl;
        cout<<"Player-"<<sorted[i].playerName<<"'s score is "<<sorted[i].score<<endl;
    }

            
    cout<<endl<<endl;
}

//Finalized ranks after completion of game by all players
void finalRanks(vector<GameOfDice> &sorted,int n)
{
    sort(sorted.begin(),sorted.end());


    for(int i=0;i<n;i++)
    {
        sorted[i].rank=i+1;
        cout<<"Rank "<<sorted[i].rank<<":";
        cout<<"Player-"<<sorted[i].playerName<<endl;
    }

    cout<<endl;

}

int main(){
    int n,m;

    static int nextTurn;
    static int countOfCompleted=0;

    //Get the number of players and threshold score from the user 

    cout<<"Enter the Number of PLayers :"<<endl;
    cin>>n;

    while(n<2){
        cout<<"The number of Players can't be less han two ,Please enter the number of players again !"<<endl;
        cin>>n;

    }

    cout<<"Enter the max Score for a player to complete a game :"<<endl;
    cin>>m;

    //Edge case for score
    while(m<2){
        cout<<"The score cannot be less than 0 , PLease enter the max score again !"<<endl;
        cin>>m;

    }

    cout<<"The Number of players are "<<n<<" and the max score for each player to complete a game is "<<m<<endl;


    vector<GameOfDice> players;
  
    cout<<"Order of Players :\n\n";

    vector<int> order= getRandomOrder(n);

    for(int i=0;i<n;i++)
    {
        cout<<i+1<<"  Player-"<<order[i]<<endl;
    }
    
    cout<<endl;

    //Assign the random order as playerNames to the players
    /*
        This makes it easy to traverse using indices
    */
    for(int i=0;i<n;i++){
        GameOfDice player=GameOfDice(order[i]);
        players.push_back(player);
    }

    

    nextTurn=0; // Initially the first player in the Random Order will start the game

    while(countOfCompleted < n )
    {
        int currPlayer = players[nextTurn].playerName;
        
        int currPlayerIndex=nextTurn;

        //If the player has rolled 1 twice consecutively in the previous turns he misses this turn

        if(players[currPlayerIndex].rollOne == 2)
        {
            cout<<"Player-"<<currPlayer<<" rolled 1 twice consecutively and hence skips the turn\n"<<endl;
            
            players[currPlayerIndex].rollOne=0;
            
            nextTurn=(currPlayerIndex + 1) % n ;
            
            continue;

        }

        //If a player has completed the game also skips the chance

        if(players[currPlayerIndex].isCompleted)
        {
            nextTurn=(currPlayerIndex + 1) % n ;
            
            continue;

        }

        cout<<"It is Player-"<<currPlayer<<"'s turn now !\n"<<endl;

        char toCont;

        cout<<"Press 'r' to continue \n\n";

        cin>>toCont;

        if(toCont=='r'){

            int currRoll = getDieRoll();


            //Keep track of two consecutive one's

            if(players[currPlayerIndex].rollOne == 1 && currRoll == 1 || currRoll == 1)
            {
                players[currPlayerIndex].rollOne++;
            }
            else
            {
                players[currPlayerIndex].rollOne=0;
            }


            cout<<"Player-"<<currPlayer<<" rolled "<<currRoll<<" .\n"<<endl;

            //Update score after every roll

            players[currPlayerIndex].score+=currRoll;

            players[currPlayerIndex].rankScore+=currRoll;


            //If the player has reached the threshold he completes the game

            if(players[currPlayerIndex].score >= m)
            {
                players[currPlayerIndex].isCompleted=true;

                players[currPlayerIndex].rankScore=m;

                cout<<"\n Player-"<<currPlayer<<" has completed the game\n"<<endl;

                countOfCompleted++;
            }

            printScores(players,n);

            // If the player has rolled 6 get's another turn 

            if(currRoll == 6){

                cout<<"\n Player-"<<currPlayer<<" has rolled 6 , hence gets another chance\n"<<endl;
                continue;
            }

            nextTurn=(currPlayerIndex + 1) % n ;
        }

        else
        {
            cout<<"You have not entered the correct key :(\n\n";
        }
       

    }

    cout<<"The Final Scores :\n";

    finalRanks(players,n);

    cout<<"Congrats ! The game is over :) !"<<endl;
    


}